#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   @meitingli
#@Contact   	:   bubble_mt@outlook.com
#@Date      	:   2021-01-25
#@License   	:   Mulan PSL v2
#@Desc      	:   Inject create same name directory and file on mnt_point
#####################################
# shellcheck disable=SC1091

source ../common_lib/fsio_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the database config."
    mapfile -t point_list < <(CREATE_FS "$@")
    LOG_INFO "End to prepare the database config."
}

function run_test() {
    LOG_INFO "Start to run test."
    for i in $(seq 1 $((${#point_list[@]} - 1))); do
        mnt_point=${point_list[$i]}
        file=$(find / -name ls)
        nohup cp "$file" "$mnt_point"/testSame &
        nohup mkdir "$mnt_point"/testSame &
        cat "$mnt_point"/testSame
        CHECK_RESULT $? 1 0 "Cat same name file unexpectly."
        cd "$mnt_point"/testSame || exit
        CHECK_RESULT $? 1 0 "cd same name directory unexpectly."
    done
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    list=${point_list[*]}
    REMOVE_FS "$list"
    LOG_INFO "End to restore the test environment."
}

main "$@"
