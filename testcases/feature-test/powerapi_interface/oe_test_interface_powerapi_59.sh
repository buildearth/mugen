#!/usr/bin/bash

# Copyright (c) 2024. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author        :   erin_dan
#@Contact       :   1743994506@qq.com
#@Date          :   2024-07-10
#@License       :   Mulan PSL v2
#@Desc          :   PWR_CreateDcTask() param about boundary value test
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "powerapi-devel gcc"
    cp common/demo_main.c ./
    echo "int main(int argc, const char *args[])
{
    TEST_PWR_Register();
    TEST_PWR_COM_DcTaskMgr_1();
    TEST_PWR_UnRegister();
    return 0;
}
" >> demo_main.c
    gcc ./demo_main.c -o demo_main -lpwrapi
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    ./demo_main > test.log
    grep -A1 "PWR_CreateDcTask        :SUCCESS ret: 0" test.log | grep "interval:1000"
    CHECK_RESULT $? 0 0 "Check interface PWR_CreateDcTask with Param interval is 1000 failed"
    grep -A1 "PWR_CreateDcTask        :SUCCESS ret: 0" test.log | grep "interval:100000000"
    CHECK_RESULT $? 0 0 "Check interface PWR_CreateDcTask with Param interval is 100000000 failed"
    grep -A1 "PWR_CreateDcTask        :ERROR   ret: 6" test.log | grep "interval:999"
    CHECK_RESULT $? 0 0 "Check interface PWR_CreateDcTask with Param interval is 999 failed"
    grep -A1 "PWR_CreateDcTask        :ERROR   ret: 6" test.log | grep "interval:100000001"
    CHECK_RESULT $? 0 0 "Check interface PWR_CreateDcTask with Param interval is 100000001 failed"
    grep -A1 "PWR_DeleteDcTask        :SUCCESS ret: 0" test.log | grep "dataType:1"
    CHECK_RESULT $? 0 0 "Check interface PWR_DeleteDcTask with Param dataType is 1 failed"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    rm -rf demo_main* /root/pwrclient.sock test.log
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
