#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   mataotao
# @Contact   :   mataotao@uniontech.com
# @Date      :   2023-04-20
# @License   :   Mulan PSL v2
# @Desc      :   Command test-shopt
# ############################################
source "${OET_PATH}"/libs/locallibs/common_lib.sh

function run_test() {
    LOG_INFO "Start to run test."
    shopt -p
    CHECK_RESULT $? 0 0 "run shopt failed"
    shopt --help |grep "选项：" || shopt --help |grep "Options:"
    CHECK_RESULT $? 0 0 "run shopt --help failed"
    shopt -s cdspell
    res=$(shopt -s |grep cdspell |awk '{ print$2 }')
    [[ $res =~ "on" ]]
    CHECK_RESULT $? 0 0 "run shopt -s cdspell failed"
    shopt -u cdspell
    res=$(shopt -u |grep cdspell |awk '{ print$2 }')
    [[ $res =~ "off" ]]
    CHECK_RESULT $? 0 0 "run shopt -s cdspell failed"
}

main "$@"