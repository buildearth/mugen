#!/usr/bin/bash

#Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   xiechangyan1
# @Contact   :   xiechangyan@uniontech.com
# @Date      :   2023-05-29
# @License   :   Mulan PSL v2
# @Desc      :   Use php case
# ############################################

source "${OET_PATH}/libs/locallibs/common_lib.sh"

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "php"
    pwd=$(pwd)
    mkdir php_test && cd php_test || exit
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    cat > hello.php << EOF
<?php
echo "hello world !"
?>
EOF
    CHECK_RESULT $? 0 0 "Error,Fail to create hello.php"
    php hello.php > log 2>&1
    grep "hello world !" log
    CHECK_RESULT $? 0 0 "Error,check hello.php"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    cd "$pwd" && rm -rf php_test
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"