#!/usr/bin/bash

# Copyright (c) 2023. huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   sunqingwei
# @Contact   :   sunqingwei@uniontech.com
# @Date      :   2023/04/10
# @License   :   Mulan PSL v2
# @Desc      :   Test strust function
# #############################################
# shellcheck disable=SC2002,SC2035
source "$OET_PATH/libs/locallibs/common_lib.sh"

function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "struts tar binutils dnf-plugins-core"
    mkdir /tmp/test
    cd /tmp/test || exit
    old_lc_time=$(locale | grep LC_TIME | awk -F= '{print $2}' | tr -d '"')
    export LC_TIME=en_US.UTF-8
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    wget https://mirrors.huaweicloud.com/kunpeng/archive/kunpeng_solution/bigdata/Tools/checkSo.zip
    unzip checkSo.zip
    test -e checkSo
    CHECK_RESULT $? 0 0 "Decompression failure"
    yum download struts
    cd checkSo/ || exit;chmod 755 *
    ./main.sh /tmp/test/struts*.rpm | grep 'unpack file /checkSo/tmp_unzip/struts';testtime=$(date '+%a %b')
    CHECK_RESULT $? 0 0 "Execution failure"
    [[ $(cat JarResult.log |wc -l) -eq 0 ]]
    CHECK_RESULT $? 0 0 "Execution failure2"
    grep "$testtime" NonJarResult.log
    CHECK_RESULT $? 0 0 "Execution failure3"
    LOG_INFO "End to run test."
}

function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE "$@"
    rm -rf /tmp/test
    unset testtime
    export LC_TIME=${old_lc_time}
    LOG_INFO "End to restore the test environment."
}

main "$@"

