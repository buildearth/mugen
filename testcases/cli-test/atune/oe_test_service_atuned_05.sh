#!/usr/bin/bash

# Copyright (c) 2023 Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   gaoshuaishuai
# @Contact   :   gaoshuaishuai@uniontech.com
# @Date      :   2023.4.4
# @License   :   Mulan PSL v2
# @Desc      :   File system common command test-atune-version
# ############################################

source "$OET_PATH/libs/locallibs/common_lib.sh"
source "./common/common_lib.sh"

function pre_test(){
    LOG_INFO "Start environment preparation."
    deploy_autund
    LOG_INFO "End of environmental preparation!"
}

function run_test() {
    array=("--version" "-v")
    for ((i=0;i<${#array[@]};i++));do
        atune-adm ${array[i]} > temp.log
        CHECK_RESULT $? 0 0 "atune-adm command execution failed"
        grep "atune-adm version *" temp.log
        CHECK_RESULT $? 0 0 "atune-adm version nonexistence"
    done
}
function post_test() {
    clean_autund
    rm -fr temp.log
}

main $@
