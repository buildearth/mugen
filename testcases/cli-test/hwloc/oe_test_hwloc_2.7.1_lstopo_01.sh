#!/usr/bin/bash

# Copyright (c) 2022. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more detaitest -f.

##############################################
#@Author    :   zhangshaowei
#@Contact   :   756800989@qq.com
#@Date      :   2022/07/26
#@License   :   Mulan PSL v2
#@Desc      :   Test lstopo
##############################################

source "${OET_PATH}/libs/locallibs/common_lib.sh"

function pre_test()
{
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "hwloc"
    LOG_INFO "End to prepare the test environment."
}

function run_test() {
    LOG_INFO "Start to run test."
    lstopo --cpukinds 
    CHECK_RESULT $? 0 0 "lstopo --cpukinds failed"
    lstopo --memattrs | grep "Memory attribute "
    CHECK_RESULT $? 0 0 "lstopo --memattrs failed"
    lstopo --no-smt | grep "PU"
    CHECK_RESULT $? 1 0 "lstopo --no-smt failed"
    lstopo --filter Core:all | grep "Core"
    CHECK_RESULT $? 0 0 "lstopo --filter failed"
    lstopo --filter Core:none | grep "Core"
    CHECK_RESULT $? 1 0 "lstopo --filter failed"
    lstopo --disallowed | grep "Package"
    CHECK_RESULT $? 0 0 "lstopo --disallowed failed"
    lstopo --allow local | grep "PU"
    CHECK_RESULT $? 0 0 "lstopo --allow failed"
    lstopo -p --flags 2 | grep "Core"
    CHECK_RESULT $? 0 0 "lstopo --flags <n> failed"
    lstopo out.xml --distances && [ -e "out.xml" ]
    CHECK_RESULT $? 0 0 "lstopo --distances failed"
    lstopo -l --distances-transform links | grep "PU"
    CHECK_RESULT $? 0 0 "lstopo --distances-transform failed"
    lstopo -h | grep "Usage: lstopo"
    CHECK_RESULT $? 0 0 "lstopo -h failed"
    LOG_INFO "Finish test!"
}

function post_test()
{
    LOG_INFO "Start to restore the test environment."
    rm -rf out.xml
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
